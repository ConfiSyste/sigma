﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFA_TESTE
{
    public partial class CADASTRO : Form
    {
        public CADASTRO()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //new Banco().InserirTeste(textBox1.Text);
        }

        private void gbDados_Enter(object sender, EventArgs e)
        {

        }

        private void CADASTRO_Load(object sender, EventArgs e)
        {
            




        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (
                txtNome.Text.Isnull()
                || txtEmail.Text.Isnull()
                || txtCep.Text.Isnull()
                || txtNumero.Text.Isnull()
                || txtUsuario.Text.Isnull()
                || txtSenha1.Text.Isnull()
                || txtSenha2.Text.Isnull()
                )
            {
                MessageBox.Show(@"Há campos incompletos");
            }
            else if (
                txtSenha1.Text != txtSenha2.Text
                )

            {

                MessageBox.Show(@"Senhas incompatíveis");
            }

            else
            {
                MessageBox.Show(@"Cadastro realizado com sucesso!");


                BancoConexaoCadastro.InserirUsuario(new BancoCadastroGs
                {
                    CadastroAlunosNome = txtNome.Text,
                    CadastroAlunosEmail = txtEmail.Text,
                    CadastroAlunosTelefone = txtTelefone.Text.ParseToRemoveFormats(),
                    CadastroAlunosCelular = txtCelular.Text.ParseToRemoveFormats(),
                    CadastroAlunosCep = txtCep.Text.ParseToRemoveFormats(),
                    CadastroAlunosNumero = txtNumero.Text.ParseToInt(),
                    CadastroAlunosUsuario = txtUsuario.Text,
                    CadastroAlunosSenha= txtSenha1.Text
                });



            }



        }





        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Close();
            new Login.Login().Show();
          
        }

        private void cbbCurso_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cbbTurma_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblCurso_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblTurma_Click(object sender, EventArgs e)
        {

        }
    }
}
