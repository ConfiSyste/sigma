﻿using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Business
{
    public class BsOnibus
    {
        public List<DTO.DtoOnibus> Carregar() { return new Database.DbOnibus().Carregar(); }
    }
}