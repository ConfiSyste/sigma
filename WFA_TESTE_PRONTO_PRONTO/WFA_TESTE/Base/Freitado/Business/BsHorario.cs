﻿using System;
using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Business
{
    public class BsHorario
    {
        public List<DTO.DtoHorario> Carregar() { return new Database.DbHorario().Carregar(); }
    }
}