﻿using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Business
{
    public class BsViagem
    {
        public void Inserir(DTO.DtoViagem obj) { new Database.DbViagem().Inserir(obj); }
        public List<DTO.DtoViagem> Carregar() { return new Database.DbViagem().Carregar(); }
    }
}