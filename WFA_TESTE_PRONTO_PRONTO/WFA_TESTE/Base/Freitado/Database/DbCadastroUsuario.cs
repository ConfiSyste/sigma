﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Database
{
    public class DbCadastroUsuario
    {
        public int Salvar(DTO.DTOCadLogin s)
        {
            string script =@"
                          INSERT INTO `TESTE`.cadastro_alunos
                        (

                            Cadastro_Alunos_Nome,
                            Cadastro_Alunos_Email,
                            Cadastro_Alunos_IdCurso,
                            Cadastro_Alunos_IdTurma,
                            Cadastro_Alunos_Telefone,
                            Cadastro_Alunos_Celular,
                            Cadastro_Alunos_Cep,
                            Cadastro_Alunos_Numero,
                            Cadastro_Alunos_Usuario,
                            Cadastro_Alunos_Senha

                        ) VALUES
                        (

                            @Cadastro_Alunos_Nome,
                            @Cadastro_Alunos_Email,
                            @Cadastro_Alunos_IdCurso,
                            @Cadastro_Alunos_IdTurma,
                            @Cadastro_Alunos_Telefone,
                            @Cadastro_Alunos_Celular,
                            @Cadastro_Alunos_Cep,
                            @Cadastro_Alunos_Numero,
                            @Cadastro_Alunos_Usuario,
                            @Cadastro_Alunos_Senha

                        )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Cadastro_Alunos_Nome",    s.CadastroAlunosNome));
            parms.Add(new MySqlParameter("Cadastro_Alunos_Email",   s.CadastroAlunosEmail));
            parms.Add(new MySqlParameter("Cadastro_Alunos_IdCurso", s.CadastroAlunosIdCurso));
            parms.Add(new MySqlParameter("Cadastro_Alunos_IdTurma", s.CadastroAlunosIdTurma ));
            parms.Add(new MySqlParameter("Cadastro_Alunos_Telefone",s.CadastroAlunosTelefone));
            parms.Add(new MySqlParameter("Cadastro_Alunos_Celular", s.CadastroAlunosCelular));
            parms.Add(new MySqlParameter("Cadastro_Alunos_Cep", s.CadastroAlunosCep));
            parms.Add(new MySqlParameter("Cadastro_Alunos_Numero", s.CadastroAlunosNumero));
            parms.Add(new MySqlParameter("Cadastro_Alunos_Usuario", s.CadastroAlunosUsuario));
            parms.Add(new MySqlParameter("Cadastro_Alunos_Senha", s.CadastroAlunosSenha));

            WFA_TESTE.Database db = new WFA_TESTE.Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }

        internal static void Salvar(DbCadastroViagem dbCadastroViagem)
        {
            throw new NotImplementedException();
        }






        //67238416891
        public List<DTO.DTOCadLogin> Consultar(string CadastroAlunosNome)
        {
            string script =
            @"SELECT * 
              FROM cadastro_alunos 
              WHERE Cadastro_Alunos_Nome like @Cadastro_Alunos_Nome                 
";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Cadastro_Alunos_Nome", string.Format("%{0}%", CadastroAlunosNome)));

           WFA_TESTE.Database db = new WFA_TESTE.Database();
            MySqlDataReader r = db.ExecuteSelectScript(script, parms);
      
            List<DTO.DTOCadLogin> login = new List<DTO.DTOCadLogin>();

            while (r.Read())
            {
                login.Add(new DTO.DTOCadLogin
                {
                    CadastroAlunosNome = r.GetString("Cadastro_Alunos_Nome"),
                    CadastroAlunosEmail = r.GetString("Cadastro_Alunos_Email"),
                    CadastroAlunosIdCurso = r.GetInt32("Cadastro_Alunos_IdCurso"),
                    CadastroAlunosIdTurma = r.GetInt32("Cadastro_Alunos_IdTurma"),
                    CadastroAlunosTelefone = r.GetString("Cadastro_Alunos_Telefone"),
                    CadastroAlunosCelular = r.GetString("Cadastro_Alunos_Celular"),
                    CadastroAlunosCep = r.GetString("Cadastro_Alunos_Cep"),
                    CadastroAlunosNumero = r.GetInt32("Cadastro_Alunos_Numero"),
                    CadastroAlunosUsuario = r.GetString("Cadastro_Alunos_Usuario"),
                    CadastroAlunosSenha = r.GetString("Cadastro_Alunos_Senha")

                });
            }

            r.Close();

            return login;
        }


    }
}
