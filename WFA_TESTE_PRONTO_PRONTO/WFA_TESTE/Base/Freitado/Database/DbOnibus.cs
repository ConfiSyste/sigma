﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Database
{
    internal class DbOnibus
    {
        internal List<DTO.DtoOnibus> Carregar()
        {
            string qry =
           @"   SELECT
                    ID_ONIBUS,
                    DS_MARCA,
                    DS_MODELO
                FROM
                    TB_ONIBUS";

            WFA_TESTE.Database dBase = new WFA_TESTE.Database();
            List<DTO.DtoOnibus> lReturn = new List<DTO.DtoOnibus>();
            using (MySqlDataReader dReader = dBase.ExecuteSelectScript(qry, null))
                while (dReader.Read())
                    lReturn.Add(new DTO.DtoOnibus
                    {
                        id_onibus = dReader.GetInt32("ID_ONIBUS"),
                        ds_marca = dReader.GetString("DS_MARCA"),
                        ds_modelo = dReader.GetString("DS_MODELO")
                    });

            return lReturn;
        }
    }
}