﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Database
{
    internal class DbViagem
    {
        internal void Inserir(DTO.DtoViagem obj)
        {
            string qry = @"
                INSERT INTO TB_VIAGEM
                (
                    ID_HORARIO,
                    ID_ONIBUS,
                    ID_MOTORISTA,
                    DS_ENDERECO,
                    NR_NUMERO
                )
                VALUES
                (
                    @ID_HORARIO,
                    @ID_ONIBUS,
                    @ID_MOTORISTA,
                    @DS_ENDERECO,
                    @NR_NUMERO
                )";

            List<MySqlParameter> lParams = new List<MySqlParameter>();
            lParams.Add(new MySqlParameter("ID_HORARIO", obj.id_horario));
            lParams.Add(new MySqlParameter("ID_ONIBUS", obj.id_onibus));
            lParams.Add(new MySqlParameter("ID_MOTORISTA", obj.id_motorista));
            lParams.Add(new MySqlParameter("DS_ENDERECO", obj.ds_endereco));
            lParams.Add(new MySqlParameter("NR_NUMERO", obj.nr_numero));

            WFA_TESTE.Database dBase = new WFA_TESTE.Database();
            dBase.ExecuteInsertScript(qry, lParams);
        }

        internal List<DTO.DtoViagem> Carregar()
        {
            string qry =
           @"   SELECT
                    V.ID_VIAGEM,

                    V.ID_HORARIO,
                    H.DS_PARTIDA,
                    H.DS_CHEGADA,

                    V.ID_ONIBUS,
                    O.DS_MARCA,
                    O.DS_MODELO,

                    V.ID_MOTORISTA,
                    M.DS_NOME,
                    M.NR_IDADE,

                    V.DS_ENDERECO,
                    V.NR_NUMERO
                FROM
                    TB_VIAGEM V
                    INNER JOIN TB_HORARIO H ON H.ID_HORARIO = V.ID_HORARIO
                    INNER JOIN TB_ONIBUS O ON O.ID_ONIBUS = V.ID_ONIBUS
                    INNER JOIN TB_MOTORISTA M ON M.ID_MOTORISTA = V.ID_MOTORISTA";

            WFA_TESTE.Database dBase = new WFA_TESTE.Database();
            List<DTO.DtoViagem> lReturn = new List<DTO.DtoViagem>();
            using (MySqlDataReader dReader = dBase.ExecuteSelectScript(qry, null))
                while (dReader.Read())
                    lReturn.Add(new DTO.DtoViagem
                    {
                        id_viagem = dReader.GetInt32("ID_VIAGEM"),

                        id_horario = dReader.GetInt32("ID_HORARIO"),
                        ds_horario = "Partida: " + dReader.GetString("DS_PARTIDA") + ", Chegada: " + dReader.GetString("DS_CHEGADA"),

                        id_onibus = dReader.GetInt32("ID_ONIBUS"),
                        ds_onibus = dReader.GetString("DS_MARCA") + " - " + dReader.GetString("DS_MODELO"),

                        id_motorista = dReader.GetInt32("ID_MOTORISTA"),
                        ds_motorista = dReader.GetString("DS_NOME") + ", " + dReader.GetInt32("NR_IDADE") + " anos",

                        ds_endereco = dReader.GetString("DS_ENDERECO"),
                        nr_numero = dReader.GetInt32("NR_NUMERO")
                    });

            return lReturn;
        }
    }
}