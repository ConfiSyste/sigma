﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Database
{
    internal class DbMotorista
    {
        internal List<DTO.DtoMotorista> Carregar()
        {
            string qry =
           @"   SELECT
                    ID_MOTORISTA,
                    DS_NOME,
                    NR_IDADE
                FROM
                    TB_MOTORISTA";

            WFA_TESTE.Database dBase = new WFA_TESTE.Database();
            List<DTO.DtoMotorista> lReturn = new List<DTO.DtoMotorista>();
            using (MySqlDataReader dReader = dBase.ExecuteSelectScript(qry, null))
                while (dReader.Read())
                    lReturn.Add(new DTO.DtoMotorista
                    {
                        id_motorista = dReader.GetInt32("ID_MOTORISTA"),
                        ds_nome = dReader.GetString("DS_NOME"),
                        nr_idade = dReader.GetInt32("NR_IDADE")
                    });

            return lReturn;
        }
    }
}