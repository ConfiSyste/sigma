﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFA_TESTE.Base.Freitado.Database
{
    class DbCadastroViagem
    {
        public int Salvar(DTO.DTOLogin s)
        {
            string script = @"
                          INSERT INTO `TESTE`.nova_viagem
                        (

                            cd_viagem_horario,
                            cd_viagem_onibus,
                            cd_viagem_motorista,
                            cd_viagem_endereco,
                            cd_viagem_numero,
                            

                        ) VALUES
                        (

                            @cd_viagem_horario,
                            @cd_viagem_onibus,
                            @cd_viagem_motorista,
                            @cd_viagem_endereco,
                            @cd_viagem_numero,
                            

                        )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cd_viagem_horario", s.cdviagemhorario));
            parms.Add(new MySqlParameter("cd_viagem_onibus", s.cdviagemonibus));
            parms.Add(new MySqlParameter("cd_viagem_motorista", s.cdviagemmotorista));
            parms.Add(new MySqlParameter("cd_viagem_endereco", s.cdviagemendereco));
            parms.Add(new MySqlParameter("cd_viagem_numero", s.cdviagemnumero));
            
            WFA_TESTE.Database db = new WFA_TESTE.Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }
    }
}
