﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace WFA_TESTE.Base.Freitado.Database
{
    internal class DbHorario
    {
        internal List<DTO.DtoHorario> Carregar()
        {
            string qry =
           @"   SELECT
                    ID_HORARIO,
                    DS_PARTIDA,
                    DS_CHEGADA
                FROM
                    TB_HORARIO";

            WFA_TESTE.Database dBase = new WFA_TESTE.Database();
            List<DTO.DtoHorario> lReturn = new List<DTO.DtoHorario>();
            using (MySqlDataReader dReader = dBase.ExecuteSelectScript(qry, null))
                while (dReader.Read())
                    lReturn.Add(new DTO.DtoHorario
                    {
                        id_horario = dReader.GetInt32("ID_HORARIO"),
                        ds_partida = dReader.GetString("DS_PARTIDA"),
                        ds_chegada = dReader.GetString("DS_CHEGADA")
                    });

            return lReturn;
        }
    }
}