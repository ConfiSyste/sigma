﻿namespace WFA_TESTE.Base.Freitado.DTO
{
    public class DtoViagem
    {
        public int id_viagem { get; set; }

        public int id_horario { get; set; }
        public string ds_horario { get; set; }

        public int id_onibus { get; set; }
        public string ds_onibus { get; set; }

        public int id_motorista { get; set; }
        public string ds_motorista { get; set; }

        public string ds_endereco { get; set; }
        public int nr_numero { get; set; }
    }
}