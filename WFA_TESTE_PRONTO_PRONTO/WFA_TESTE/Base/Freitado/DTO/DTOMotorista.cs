﻿namespace WFA_TESTE.Base.Freitado.DTO
{
    public class DtoMotorista
    {
        public int id_motorista { get; set; }
        public string ds_nome { get; set; }
        public int nr_idade { get; set; }
        public string ds_nome_idade => ds_nome + ", " + nr_idade + " anos";
    }
}