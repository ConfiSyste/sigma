﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WFA_TESTE.Base.Freitado.DTO
{ 

        #region DTOCurso
        public class DTOCurso
        {
            public int CadastroAlunosIdCurso { get; set; }
            public string CadastroAlunosNmCurso { get; set; }
        }
        #endregion
        #region DTOTurma
        public class DTOTurma
        {
            public int CadastroAlunosIdTurma { get; set; }
            public string CadastroAlunosNmTurma { get; set; }
        }
        #endregion
        #region DTOCadLogin
        public class DTOCadLogin
        {

        
            public string CadastroAlunosNome { get; set; }
            public string CadastroAlunosEmail { get; set; }
            public int CadastroAlunosIdCurso { get; set; }
            public int CadastroAlunosIdTurma { get; set; }
            public string CadastroAlunosTelefone { get; set; }
            public string CadastroAlunosCelular { get; set; }
            public string CadastroAlunosCep { get; set; }
            public int CadastroAlunosNumero { get; set; }
            public string CadastroAlunosUsuario { get; set; }
            public string CadastroAlunosSenha { get; set; }
        }
        #endregion
 
    
    
    
          
}