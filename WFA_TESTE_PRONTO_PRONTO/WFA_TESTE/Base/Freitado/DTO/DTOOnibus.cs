﻿namespace WFA_TESTE.Base.Freitado.DTO
{
    public class DtoOnibus
    {
        public int id_onibus { get; set; }
        public string ds_marca { get; set; }
        public string ds_modelo { get; set; }
        public string ds_marca_modelo => ds_marca + " - " + ds_modelo;
    }
}