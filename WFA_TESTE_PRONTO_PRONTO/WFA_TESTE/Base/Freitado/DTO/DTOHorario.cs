﻿namespace WFA_TESTE.Base.Freitado.DTO
{
    public class DtoHorario
    {
        public int id_horario { get; set; }
        public string ds_partida { get; set; }
        public string ds_chegada { get; set; }
        public string ds_partida_chegada => "Partida: " + ds_partida + ", Chegada: " + ds_chegada;
    }
}