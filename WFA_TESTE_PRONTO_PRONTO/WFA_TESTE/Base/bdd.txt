DROP DATABASE IF EXISTS `TESTE`;
CREATE DATABASE `TESTE`;
USE `TESTE`;
-- Cria Tabela Horario
CREATE TABLE `tb_horario` (
  `id_horario` int(11) NOT NULL AUTO_INCREMENT,
  `ds_partida` varchar(5) NOT NULL,
  `ds_chegada` varchar(5) NOT NULL,
  PRIMARY KEY (`id_horario`));
-- Cria tabela onibus
CREATE TABLE `tb_onibus` (
  `id_onibus` int(11) NOT NULL AUTO_INCREMENT,
  `ds_marca` varchar(50) NOT NULL,
  `ds_modelo` varchar(50) NOT NULL,
  PRIMARY KEY (`id_onibus`));
-- Cria tabela motorista
CREATE TABLE `tb_motorista` (
  `id_motorista` int(11) NOT NULL AUTO_INCREMENT,
  `ds_nome` varchar(50) NOT NULL,
  `nr_idade` int NOT NULL,
  PRIMARY KEY (`id_motorista`));
  -- Insere dados nas tabelas horario, onibus e motorista
INSERT INTO `tb_horario` VALUES (1,'05:30','13:35'),(2,'5:45','13:20'),(3,'6:00','13:05'),(4,'6:15','12:50'),(5,'6:30','12:35');
INSERT INTO `tb_onibus` VALUES (1,'CAIO','Apache Vip I'),(2,'CAIO','Milennium BRT'),(3,'Marcopolo','Paradiso G7 1200');
INSERT INTO `tb_motorista` VALUES (1,'Diego Santos',16),(2,'Rodrigo da Silva',16),(3,'Gustavo Oliveira',19);
-- Cria tabela viagem
CREATE TABLE `tb_viagem` (
  `id_viagem` int(11) NOT NULL AUTO_INCREMENT,
  `id_horario` int(11) NOT NULL,
  `id_onibus` int(11) NOT NULL,
  `id_motorista` int(11) NOT NULL,
  `ds_endereco` varchar(100) NOT NULL,
  `nr_numero` int(5) NOT NULL,
  PRIMARY KEY (`id_viagem`));
-- Insere dados na tabela viagem
SHOW TABLES;
DESCRIBE tb_viagem;