﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFA_TESTE
{
    public static class Extensions
    {
        public static bool Isnull(this string n)
        {
            
            return string.IsNullOrWhiteSpace(n) || string.IsNullOrEmpty(n);
        }


        public static int ParseToInt(this object n)
        {

            return Convert.ToInt32(n);
        }


        public static string ParseToString(this object n)
        {

            return Convert.ToString(n);
        }

        public static string ParseToRemoveFormats(this string n, string formats = "/-")
        {

            for (int i = 0; i < formats.Length; i++)


                    {
                n.Replace(formats[i].ToString(), "");

                    }




            return n;


        }

    }
}
