﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using WFA_TESTE;


namespace WFA_TESTE
{
    static class BancoConexaoCadastro
    {
        public static void InserirUsuario(BancoCadastroGs s)
        {
            using (var conn = new MySqlConnection("Server=localhost;Port=3306;Database=TESTE;Uid=root;Pwd=12345678;"))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"
                        INSERT INTO `TESTE`.cadastro_alunos
                        (

                            Cadastro_Alunos_Nome,
                            Cadastro_Alunos_Email,
                            Cadastro_Alunos_IdCurso,
                            Cadastro_Alunos_IdTurma,
                            Cadastro_Alunos_Telefone,
                            Cadastro_Alunos_Celular,
                            Cadastro_Alunos_Cep,
                            Cadastro_Alunos_Numero,
                            Cadastro_Alunos_Usuario,
                            Cadastro_Alunos_Senha
                            
                        ) VALUES (

                            @Cadastro_Alunos_Nome,
                            @Cadastro_Alunos_Email,
                            @Cadastro_Alunos_IdCurso,
                            @Cadastro_Alunos_IdTurma,
                            @Cadastro_Alunos_Telefone,
                            @Cadastro_Alunos_Celular,
                            @Cadastro_Alunos_Cep,
                            @Cadastro_Alunos_Numero,
                            @Cadastro_Alunos_Usuario,
                            @Cadastro_Alunos_Senha
  
                        );";
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Nome", s.CadastroAlunosNome);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Email", s.CadastroAlunosEmail);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_IdCurso", s.CadastroAlunosIdCurso);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_IdTurma", s.CadastroAlunosIdTurma);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Telefone", s.CadastroAlunosTelefone);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Celular", s.CadastroAlunosCelular);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Cep", s.CadastroAlunosCep);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Numero", s.CadastroAlunosNumero);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Usuario", s.CadastroAlunosUsuario);
                    cmd.Parameters.AddWithValue("Cadastro_Alunos_Senha", s.CadastroAlunosSenha);


                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class BancoCadastroGs

    {
       
        public string CadastroAlunosNome { get; set; }
        public string CadastroAlunosEmail { get; set; }
        public int CadastroAlunosIdCurso { get; set; }
        public int CadastroAlunosIdTurma { get; set; }
        public string CadastroAlunosTelefone { get; set; }
        public string CadastroAlunosCelular { get; set; }
        public string CadastroAlunosCep { get; set; }
        public int CadastroAlunosNumero { get; set; }
        public string CadastroAlunosUsuario { get; set; }
        public string CadastroAlunosSenha { get; set; }
    }



    public static class BancoCadastroInserir
    {
        public static void InserirUsuario(BancoCadastroGs s) { BancoConexaoCadastro.InserirUsuario(s); }
    }
}
#region ConexaoTurmas
static class BancoConexaoTurma


{

    internal static List<BancoTurmaGs> ListarTurmas(int idCurso)
    {
        using (var conn = new MySqlConnection("Server=localhost;Port=3306;Database=TESTE;Uid=root;Pwd=12345678;"))
        using (var cmd = conn.CreateCommand())
        {
                cmd.Parameters.AddWithValue("ID_CURSO", idCurso);
            cmd.CommandText = @"
                    SELECT
                        ID_TURMA,
                        NM_TURMA
                    FROM
                        `TESTE`.TB_TURMA
                    WHERE
                        ID_CURSO = @ID_CURSO
                    ORDER BY NM_TURMA";

            if (conn.State != ConnectionState.Open)
                conn.Open();

            var dReader = cmd.ExecuteReader();
            var lReturn = new List<BancoTurmaGs>();
            while (dReader.Read())
                lReturn.Add(new BancoTurmaGs
                {
                    CadastroAlunosIdTurma = dReader["ID_TURMA"].ParseToInt(),
                    CadastroAlunosNmTurma = dReader["NM_TURMA"].ParseToString()
                });

            return lReturn;
        }
    }
}
#endregion
#region GetSetTurmas

public class BancoTurmaGs

{
    public int CadastroAlunosIdTurma { get; set; }
    public string CadastroAlunosNmTurma { get; set; }
}
#endregion
#region InserirTurmas
public static class BancoTurmaInserir
{
    public static List<BancoTurmaGs> ListarTurmas(int idCurso = 0) { return BancoConexaoTurma.ListarTurmas(idCurso); }
}
#endregion

public static class BancoConexaoCurso
{
    public static List<BancoCursoGs> ListarCursos()
    {
        using (var conn = new MySqlConnection("Server=localhost;Port=3306;Database=TESTE;Uid=root;Pwd=12345678;"))
        using (var cmd = conn.CreateCommand())
        {
            cmd.CommandText = @"
                    SELECT

                        IDCURSO,
                        NMCURSO

                    FROM
                        `TESTE`.tb_curso ";

            if (conn.State != ConnectionState.Open)
                conn.Open();

            var dReader = cmd.ExecuteReader();
            var lReturn = new List<BancoCursoGs>();
            while (dReader.Read())
                lReturn.Add(new BancoCursoGs
                {
                    CadastroAlunosIdCurso = dReader["IDCURSO"].ParseToInt(),
                    CadastroAlunosNmCurso = dReader["NMCURSO"].ParseToString()
                });

            return lReturn;
        }
    }


    #region GetSetCursos
    public class BancoCursoGs
    {
        public int CadastroAlunosIdCurso { get; set; }
        public string CadastroAlunosNmCurso { get; set; }
    }
    #endregion


 



}
























